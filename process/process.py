from glob import glob
import os
import subprocess

task = 'pp' # npnp, np3
sign = 'simulation' # support, unate

for i in glob(f"../epfl_benchmark/{task}/*"):
    fn = i[i.rfind('/')+1:]
    print(fn)
    a = glob(i + '/*')
    # print(a)
    os.system(f'../abc/abc -c "bm {a[0]} {a[1]}" > {sign}/{task}/{fn}.txt')
