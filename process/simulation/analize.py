from glob import glob
import os
import subprocess
from mpmath import mpf, log10


task = 'pp'

for i in glob(f"../../epfl_benchmark/{task}/*"):
    fn = i[i.rfind('/')+1:]
    a = glob(i + '/*')
    
    
    in1 = []
    in2 = []
    on1 = []
    on2 = []

    time = 0
    with open(f'{task}/{fn}.txt', 'r') as f:
        while (line := f.readline()):
            if line.find('inps/outs') != -1:
                a = [int(i) for i in line[line.find(':') + 2:].split()]
                n = a[0]
                m = a[1]
                in1 = [0 for i in range(n)]
                in2 = [0 for i in range(n)]
                on1 = [0 for i in range(m)]
                on2 = [0 for i in range(m)]
            if line.find('OUT') != -1:
                line = f.readline().strip().split()
                for i in range(n):
                    q = f.readline().strip().split()
                    for ii in range(m):
                        if q[ii] != line[ii]:
                            in1[i] += 1
                            on1[ii] += 1
                line = f.readline().strip().split()
                for i in range(n):
                    q = f.readline().strip().split()
                    for ii in range(m):
                        if q[ii] != line[ii]:
                            in2[i] += 1
                            on2[ii] += 1

    # print(in1)
    # print(in2)
    # print(on1)
    # print(on2)

    print(n, m, sep=',')

    # all_m = mpf(1)
    # for i in range(n):
    #     all_m *= i + 1
    # for i in range(m):
    #     all_m *= i + 1
    
    # curr_m = mpf(1)
    # vals = dict()
    # for i in range(len(in1)):
    #     if vals.get(in1[i]) == None:
    #         vals[in1[i]] = 1
    #     else:
    #         vals[in1[i]] += 1
    # for i,j in vals.items():
    #     for k in range(j):
    #         curr_m *= k + 1

    # vals = dict()
    # for i in range(len(on1)):
    #     if vals.get(on1[i]) == None:
    #         vals[on1[i]] = 1
    #     else:
    #         vals[on1[i]] += 1
    # for i,j in vals.items():
    #     for k in range(j):
    #         curr_m *= k + 1
    
    
    # metric = log10(all_m / curr_m)
    # if metric < 0.001:
    #     metric = 0
    # print(time, metric, sep=',')
