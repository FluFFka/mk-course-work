from glob import glob
import os
import subprocess
from mpmath import mpf, log10


task = 'npnp'

for i in glob(f"../../epfl_benchmark/{task}/*"):
    fn = i[i.rfind('/')+1:]
    a = glob(i + '/*')
    
    un1 = []
    un2 = []
    on1 = []
    on2 = []
    on = []

    time = 0
    with open(f'{task}/{fn}.txt', 'r') as f:
        while (line := f.readline()):
            if line.find('Total') != -1 and line.find('sec') != -1:
                l = line.find('=')
                r = line.find('sec')
                time += float(line[l+1:r])
            if line.find('Shared BDD') != -1:
                un = []
                while (line := f.readline()):
                    if line.find('Out ') == -1:
                        if len(un1) == 0:
                            un1 = un
                            for j in on:
                                on1.append(tuple(j))
                        else:
                            un2 = un
                            for j in on:
                                on2.append(tuple(j))
                        n, m = [int(i) for i in line[line.find('=')+1 : line.find('.')].split('/')]
                        # Ins/Outs =  256/ 129.
                        on = []
                        break
                    if line[-1] == '\n':
                        line = line[:-1]
                    line = line[line.find(':') + 2:]
                    # print(line)
                    el = [0, 0, 0]
                    sym_n = 0
                    for sym in line:
                        if len(on) < sym_n + 1:
                            on.append([0, 0, 0])
                        if sym == '.':
                            el[0] += 1
                            on[sym_n][0] += 1
                        elif sym == ' ':
                            el[1] += 1
                            el[2] += 1
                            on[sym_n][1] += 1
                            on[sym_n][2] += 1
                        elif sym == 'p':
                            el[1] += 1
                            on[sym_n][1] += 1
                        elif sym == 'n':
                            el[2] += 1
                            on[sym_n][2] += 1
                        else:
                            print('wrong symbol in unateness log:', sym, ord(sym))
                            exit(1)
                        sym_n += 1
                    un.append(tuple(el))

    if len(un2) == 0:
        print('-1,-1')
    elif task == 'pp':
        all_m = mpf(1)
        for i in range(n):
            all_m *= i + 1
        for i in range(m):
            all_m *= i + 1
        
        curr_m = mpf(1)
        vals = dict()
        for i in range(len(un1)):
            if vals.get(un1[i]) == None:
                vals[un1[i]] = 1
            else:
                vals[un1[i]] += 1
        for i,j in vals.items():
            for k in range(j):
                curr_m *= k + 1

        vals = dict()
        for i in range(len(on1)):
            if vals.get(on1[i]) == None:
                vals[on1[i]] = 1
            else:
                vals[on1[i]] += 1
        for i,j in vals.items():
            for k in range(j):
                curr_m *= k + 1
        
        
        metric = log10(all_m / curr_m)
        if metric < 0.001:
            metric = 0
        print(time, metric, sep=',')
    else:
        all_m = mpf(2) ** (n + m)
        for i in range(n):
            all_m *= i + 1
        for i in range(m):
            all_m *= i + 1
        
        curr_m = mpf(1)
        vals = dict()
        for i in range(len(un1)):
            new_k = (un1[i][0], un1[i][1] + un1[i][2])
            if vals.get(new_k) == None:
                vals[new_k] = 1
            else:
                vals[new_k] += 1
        for i,j in vals.items():
            for k in range(j):
                curr_m *= k + 1

        metric = log10(all_m / curr_m)
        if metric < 0.001:
            metric = 0
        print(time, metric, sep=',')