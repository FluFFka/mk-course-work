from glob import glob
import os
import subprocess
from mpmath import mpf, log10


task = 'np3'

for i in glob(f"../../epfl_benchmark/{task}/*"):
    fn = i[i.rfind('/')+1:]
    a = glob(i + '/*')
    
    id1 = []
    id2 = []
    od1 = []
    od2 = []

    time = 0
    with open(f'{task}/{fn}.txt', 'r') as f:
        while (line := f.readline()):
            if line.find('TOTAL') != -1:
                l = line.find('=')
                r = line.find('sec')
                time += float(line[l+1:r])
            if line.find('Input Deps:') != -1:
                id1 = [int(i) for i in f.readline().split()]
            if line.find('Input Deps222222:') != -1:
                id2 = [int(i) for i in f.readline().split()]
            if line.find('Output Deps:') != -1:
                od1 = [int(i) for i in f.readline().split()]
            if line.find('Output Deps222222:') != -1:
                od2 = [int(i) for i in f.readline().split()]


    if task == 'pp' or task == 'npnp':
        all_m = mpf(1)
        curr_m = mpf(1)
        vals = dict()
        for i in range(len(id1)):
            all_m *= (i + 1)
            if vals.get(id1[i]) == None:
                vals[id1[i]] = 1
            else:
                vals[id1[i]] += 1
        for i,j in vals.items():
            for k in range(j):
                curr_m *= k + 1
        vals = dict()
        for i in range(len(od1)):
            all_m *= (i + 1)
            if vals.get(od1[i]) == None:
                vals[od1[i]] = 1
            else:
                vals[od1[i]] += 1
        for i,j in vals.items():
            for k in range(j):
                curr_m *= k + 1
        if task == 'npnp':
            all_m *= mpf(2)**(len(id1) + len(od1))
        metric = log10(all_m / curr_m)
        if metric < 0.001:
            metric = 0
        print(time, metric, sep=',')
    else:
        all_m = mpf(len(id1) + 2)**len(id2) * mpf(len(od1))**len(od2)
        curr_m = mpf(len(id1) + 2)**len(id2)
        for i in range(len(od1)):
            sup_num = 0
            for j in range(len(od2)):
                if od1[i] >= od2[j]:
                    sup_num += 1
            curr_m *= sup_num
        metric = log10(all_m / curr_m)
        if metric < 0.001:
            metric = 0
        print(time, metric, sep=',')
        
