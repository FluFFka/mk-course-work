ABC command line: "bm ../epfl_benchmark/np3/i2c/i2c1.v ../epfl_benchmark/np3/i2c/i2c2.v".

Warning: Constant-0 drivers added to 3 non-driven nets in network "i2c":
po031, po055, po073
Network  strashing is done!
Total func supps   =     1546.
Total struct supps =     1546.
Sat runs SAT       =        3.
Sat runs UNSAT     =        0.
Simulation   =     0.00 sec
Traversal    =     0.00 sec
Fraiging     =     0.01 sec
SAT          =     0.00 sec
TOTAL        =     0.02 sec
Total func supps   =     1134.
Total struct supps =     1454.
Sat runs SAT       =        2.
Sat runs UNSAT     =      320.
Simulation   =     0.00 sec
Traversal    =     0.00 sec
Fraiging     =     0.02 sec
SAT          =     0.01 sec
TOTAL        =     0.05 sec
Getting dependencies is done!
Input Deps:
3 2 17 41 21 20 20 21 20 21 20 21 21 20 20 17 20 20 20 20 17 20 21 2 17 6 13 13 6 6 3 3 3 3 3 3 3 3 17 7 17 17 17 17 17 17 17 17 17 17 17 9 8 13 22 1 1 2 13 6 3 2 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 2 2 2 3 27 1 2 12 2 2 3 3 3 3 3 3 3 11 11 10 3 3 12 1 1 1 1 1 11 1 1 11 14 3 2 2 2 2 14 1 2 1 3 1 21 5 1 2 2 17 1 117 1 40 41 41 1 1 48 48 48 5 5 5 5 5 5 5 5 
Input Deps222222:
0 3 10 3 3 5 8 3 2 1 5 23 2 2 17 6 2 2 3 21 2 6 17 1 2 3 2 39 0 3 97 4 3 1 12 17 2 6 0 3 17 1 3 3 1 17 1 3 47 17 3 0 11 4 1 7 47 1 2 3 3 2 2 2 3 3 17 5 1 1 2 4 2 12 2 17 2 3 18 1 1 27 47 1 2 0 11 17 3 2 17 3 5 3 2 3 5 3 2 17 2 3 13 3 3 3 1 0 1 13 40 0 3 3 3 3 0 3 40 1 17 1 4 3 12 17 2 3 3 2 5 17 2 10 17 17 3 3 3 11 2 10 2 2 12 11 2 
Output Deps:
1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 22 21 21 23 20 24 24 20 20 20 20 20 20 24 20 21 20 24 20 20 21 20 20 4 21 17 14 15 17 14 6 6 6 6 6 6 6 6 21 6 21 21 21 21 21 21 21 21 21 21 21 4 5 13 20 4 10 10 11 14 4 4 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 10 9 13 9 9 9 9 9 9 9 9 10 12 12 12 9 9 12 10 9 9 9 10 12 10 10 8 6 10 10 6 2 10 7 2 7 3 5 3 2 2 2 4 3 
Output Deps222222:
9 6 18 14 0 9 6 4 1 12 9 0 9 3 10 19 9 9 6 1 21 21 9 0 21 10 13 6 2 5 4 0 2 0 21 9 9 6 8 9 4 0 0 4 9 6 9 6 1 9 9 9 9 20 9 0 1 21 9 6 5 9 9 5 0 9 1 0 9 21 10 2 6 0 10 10 1 9 4 3 11 0 0 21 9 9 6 0 10 9 14 9 0 21 1 9 9 17 9 9 21 12 1 21 3 1 10 1 0 10 4 17 6 12 10 9 21 14 0 13 1 21 0 9 0 21 1 2 6 1 1 9 21 21 12 9 21 12 0 0 9 4 
