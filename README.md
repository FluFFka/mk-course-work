# mk-course-work

## abc
В директории `abc/` расположена копия репозитория "ABC: System for Sequential Logic Synthesis and Formal Verification" https://github.com/berkeley-abc/abc. Там же расположена инструкция по запуску проекта.

В файле `abc/src/base/abci/abcBm.c` в функции bmGateWay реализованы сигнатуры и их вывод, а так же решение соответcвтующей задачи проверки эквивалентности с помощью уменьшения пространства поиска выбранной сигнатурой и решение задачи при помощи SAT.

## Тестовые данные
Схемы, взятые из EPFL bechmark, лежат в директории `epfl_benchmark/schemas`.

Файл `epfl_benchmark/CreateFiles.ipynb` нужен для преобразования схем из бенчмарка в тестовые данные для различных задач проверки эквивалентности. Полученные наборы лежат в директориях `epfl_benchmark/pp`, `epfl_benchmark/npnp` и `epfl_benchmark/np3`.

## Запуск тестирования и анализ результатов

Программа `process/process.py` запускает выбранную сигнатуру `sign` на выбранном наборе данных `task` и сохраняет результат в `process/{sign}/{task}/`. Парсинг результатов запускается соответствующей программой `process/{sign}/analize.py`.