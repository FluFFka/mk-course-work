module top ( 
    \opcode[0] , \opcode[1] , \opcode[2] , \opcode[3] , \opcode[4] ,
    \op_ext[0] , \op_ext[1] ,
    \sel_reg_dst[0] , \sel_reg_dst[1] , \sel_alu_opB[0] , \sel_alu_opB[1] ,
    \alu_op[0] , \alu_op[1] , \alu_op[2] , \alu_op_ext[0] ,
    \alu_op_ext[1] , \alu_op_ext[2] , \alu_op_ext[3] , halt, reg_write,
    sel_pc_opA, sel_pc_opB, beqz, bnez, bgez, bltz, jump, Cin, invA, invB,
    sign, mem_write, sel_wb  );
  input  \opcode[0] , \opcode[1] , \opcode[2] , \opcode[3] , \opcode[4] ,
    \op_ext[0] , \op_ext[1] ;
  output \sel_reg_dst[0] , \sel_reg_dst[1] , \sel_alu_opB[0] ,
    \sel_alu_opB[1] , \alu_op[0] , \alu_op[1] , \alu_op[2] ,
    \alu_op_ext[0] , \alu_op_ext[1] , \alu_op_ext[2] , \alu_op_ext[3] ,
    halt, reg_write, sel_pc_opA, sel_pc_opB, beqz, bnez, bgez, bltz, jump,
    Cin, invA, invB, sign, mem_write, sel_wb;
  wire n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48,
    n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n63, n64,
    n65, n66, n67, n68, n69, n71, n72, n73, n74, n75, n76, n77, n78, n80,
    n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94,
    n95, n96, n97, n99, n100, n101, n102, n103, n104, n106, n107, n108,
    n109, n110, n111, n112, n113, n114, n115, n117, n118, n119, n120, n121,
    n123, n124, n125, n126, n127, n128, n129, n131, n132, n134, n135, n136,
    n137, n139, n140, n141, n142, n143, n145, n146, n147, n148, n149, n150,
    n151, n153, n154, n156, n158, n159, n160, n161, n162, n164, n165, n166,
    n167, n169, n170, n171, n173, n174, n175, n177, n179, n180, n181, n182,
    n183, n184, n185, n186, n188, n189, n190, n191, n192, n193, n194, n195,
    n197, n198, n200, n201, n202, n203, n205, n206, n207;
  wire iinp0, iinp1, iinp2, iinp3, iinp4, iinp5, iinp6, oout0, oout1, oout2, oout3, oout4, oout5, oout6, oout7, oout8, oout9, oout10, oout11, oout12, oout13, oout14, oout15, oout16, oout17, oout18, oout19, oout20, oout21, oout22, oout23, oout24, oout25;
buf ( iinp1 , \opcode[0] );
assign iinp6 = ~\opcode[1] ;
assign iinp3 = ~\opcode[2] ;
buf ( iinp5 , \opcode[3] );
buf ( iinp2 , \opcode[4] );
buf ( iinp4 , \op_ext[0] );
buf ( iinp0 , \op_ext[1] );
buf ( \sel_reg_dst[0] , oout16 );
buf ( \sel_reg_dst[1] , oout8 );
assign \sel_alu_opB[0] = ~oout3 ;
assign \sel_alu_opB[1] = ~oout1 ;
assign \alu_op[0] = ~oout9 ;
assign \alu_op[2] = ~oout21 ;
assign \alu_op_ext[0] = ~oout18 ;
buf ( \alu_op_ext[1] , oout13 );
buf ( \alu_op_ext[2] , oout15 );
assign \alu_op_ext[3] = ~oout23 ;
assign halt = ~oout2 ;
buf ( reg_write , oout24 );
assign sel_pc_opA = ~oout10 ;
buf ( sel_pc_opB , oout25 );
buf ( beqz , oout11 );
assign bnez = ~oout22 ;
buf ( bgez , oout5 );
buf ( bltz , oout0 );
buf ( jump , oout14 );
buf ( Cin , oout4 );
assign invA = ~oout6 ;
assign invB = ~oout20 ;
assign sign = ~oout17 ;
assign mem_write = ~oout19 ;
assign sel_wb = ~oout12 ;
asoout23 n35 = iinp0  & ~iinp1 ;
  asoout23 n36 = iinp3  & iinp4 ;
  asoout23 n37 = n35 & n36;
  asoout23 n38 = iinp1  & iinp3 ;
  asoout23 n39 = iinp4  & n38;
  asoout23 n40 = ~n37 & ~n39;
  asoout23 n41 = ~iinp2  & ~n40;
  asoout23 n42 = ~iinp1  & iinp3 ;
  asoout23 n43 = iinp4  & n42;
  asoout23 n44 = ~iinp3  & ~iinp4 ;
  asoout23 n45 = ~n36 & ~n44;
  asoout23 n46 = iinp1  & ~n45;
  asoout23 n47 = ~n43 & ~n46;
  asoout23 n48 = iinp2  & ~n47;
  asoout23 oout0  = n41 | n48;
  asoout23 n50 = ~iinp0  & ~n36;
  asoout23 n51 = ~iinp0  & ~n50;
  asoout23 n52 = ~iinp1  & ~n51;
  asoout23 n53 = ~iinp3  & ~n44;
  asoout23 n54 = iinp1  & ~n53;
  asoout23 n55 = ~n52 & ~n54;
  asoout23 n56 = ~iinp2  & ~n55;
  asoout23 n57 = ~iinp3  & iinp4 ;
  asoout23 n58 = ~iinp3  & ~n57;
  asoout23 n59 = iinp1  & ~n58;
  asoout23 n60 = iinp1  & ~n59;
  asoout23 n61 = iinp2  & ~n60;
  asoout23 oout1  = ~n56 & ~n61;
  asoout23 n63 = ~iinp0  & ~n45;
  asoout23 n64 = iinp3  & ~n36;
  asoout23 n65 = iinp0  & ~n64;
  asoout23 n66 = ~n63 & ~n65;
  asoout23 n67 = iinp1  & ~n66;
  asoout23 n68 = ~n52 & ~n67;
  asoout23 n69 = ~iinp2  & ~n68;
  asoout23 oout2  = ~iinp2  & ~n69;
  asoout23 n71 = ~iinp0  & ~iinp3 ;
  asoout23 n72 = ~n57 & n71;
  asoout23 n73 = iinp0  & ~n45;
  asoout23 n74 = ~n72 & ~n73;
  asoout23 n75 = ~iinp1  & ~n74;
  asoout23 n76 = ~n54 & ~n75;
  asoout23 n77 = ~iinp2  & ~n76;
  asoout23 n78 = iinp2  & ~n53;
  asoout23 oout3  = ~n77 & ~n78;
  asoout23 n80 = ~iinp0  & iinp3 ;
  asoout23 n81 = iinp4  & iinp5 ;
  asoout23 n82 = n80 & n81;
  asoout23 n83 = iinp3  & ~iinp6 ;
  asoout23 n84 = ~n36 & n83;
  asoout23 n85 = iinp3  & ~iinp5 ;
  asoout23 n86 = ~n36 & n85;
  asoout23 n87 = iinp3  & iinp5 ;
  asoout23 n88 = ~n86 & ~n87;
  asoout23 n89 = iinp6  & ~n88;
  asoout23 n90 = ~n84 & ~n89;
  asoout23 n91 = iinp0  & ~n90;
  asoout23 n92 = ~n82 & ~n91;
  asoout23 n93 = iinp1  & ~n92;
  asoout23 n94 = ~iinp2  & ~n93;
  asoout23 n95 = iinp0  & ~n53;
  asoout23 n96 = iinp0  & ~n95;
  asoout23 n97 = iinp2  & ~n96;
  asoout23 oout4  = ~n94 & ~n97;
  asoout23 n99 = iinp3  & iinp6 ;
  asoout23 n100 = ~n84 & ~n99;
  asoout23 n101 = iinp1  & ~n100;
  asoout23 n102 = ~iinp2  & ~n101;
  asoout23 n103 = iinp1  & ~n54;
  asoout23 n104 = iinp2  & ~n103;
  asoout23 oout5  = ~n102 & ~n104;
  asoout23 n106 = ~iinp1  & ~n36;
  asoout23 n107 = ~n44 & n106;
  asoout23 n108 = ~n44 & n50;
  asoout23 n109 = iinp0  & ~n58;
  asoout23 n110 = ~n108 & ~n109;
  asoout23 n111 = iinp1  & ~n110;
  asoout23 n112 = ~n107 & ~n111;
  asoout23 n113 = ~iinp2  & ~n112;
  asoout23 n114 = iinp2  & iinp3 ;
  asoout23 n115 = iinp4  & n114;
  asoout23 oout6  = n113 | n115;
  asoout23 n117 = ~iinp1  & ~iinp2 ;
  asoout23 n118 = ~n52 & n117;
  asoout23 n119 = iinp1  & ~n74;
  asoout23 n120 = ~n37 & ~n119;
  asoout23 n121 = iinp2  & ~n120;
  asoout23 oout7  = n118 | n121;
  asoout23 n123 = ~iinp0  & ~n53;
  asoout23 n124 = ~iinp0  & ~n123;
  asoout23 n125 = iinp1  & ~n124;
  asoout23 n126 = iinp1  & ~iinp2 ;
  asoout23 n127 = ~n125 & n126;
  asoout23 n128 = iinp1  & iinp2 ;
  asoout23 n129 = ~n45 & n128;
  asoout23 oout8  = n127 | n129;
  asoout23 n131 = ~n106 & ~n125;
  asoout23 n132 = ~iinp2  & ~n131;
  asoout23 oout9  = ~n61 & ~n132;
  asoout23 n134 = ~n80 & ~n109;
  asoout23 n135 = iinp1  & ~n134;
  asoout23 n136 = ~iinp2  & ~n107;
  asoout23 n137 = ~n135 & n136;
  asoout23 oout10  = ~n78 & ~n137;
  asoout23 n139 = ~iinp0  & ~n58;
  asoout23 n140 = ~iinp0  & ~n139;
  asoout23 n141 = ~iinp1  & ~n140;
  asoout23 n142 = ~iinp1  & ~n141;
  asoout23 n143 = ~iinp2  & ~n142;
  asoout23 oout11 = ~iinp2  & ~n143;
  asoout23 n145 = ~iinp1  & ~n134;
  asoout23 n146 = ~n59 & ~n145;
  asoout23 n147 = ~iinp2  & ~n146;
  asoout23 n148 = ~iinp1  & iinp4 ;
  asoout23 n149 = iinp1  & ~n64;
  asoout23 n150 = ~n148 & ~n149;
  asoout23 n151 = iinp2  & ~n150;
  asoout23 oout12 = n147 | n151;
  asoout23 n153 = iinp0  & ~n109;
  asoout23 n154 = iinp2  & ~n153;
  asoout23 oout13 = iinp2  & ~n154;
  asoout23 n156 = iinp2  & ~n140;
  asoout23 oout14 = iinp2  & ~n156;
  asoout23 n158 = ~iinp0  & ~n64;
  asoout23 n159 = ~iinp0  & ~n158;
  asoout23 n160 = ~iinp1  & ~n159;
  asoout23 n161 = ~iinp1  & ~n160;
  asoout23 n162 = iinp2  & ~n161;
  asoout23 oout15 = iinp2  & ~n162;
  asoout23 n164 = iinp0  & ~n65;
  asoout23 n165 = ~iinp1  & ~n164;
  asoout23 n166 = ~iinp1  & ~n165;
  asoout23 n167 = iinp2  & ~n166;
  asoout23 oout16 = iinp2  & ~n167;
  asoout23 n169 = iinp1  & ~n164;
  asoout23 n170 = iinp1  & ~n169;
  asoout23 n171 = iinp2  & ~n170;
  asoout23 oout17 = iinp2  & ~n171;
  asoout23 n173 = iinp1  & ~n159;
  asoout23 n174 = iinp1  & ~n173;
  asoout23 n175 = iinp2  & ~n174;
  asoout23 oout18 = iinp2  & ~n175;
  asoout23 n177 = iinp2  & ~n58;
  asoout23 oout19 = iinp2  & ~n177;
  asoout23 n179 = iinp0  & iinp1 ;
  asoout23 n180 = ~n88 & n179;
  asoout23 n181 = n35 & ~n65;
  asoout23 n182 = ~iinp2  & ~n181;
  asoout23 n183 = ~n180 & n182;
  asoout23 n184 = iinp1  & ~n51;
  asoout23 n185 = ~n106 & ~n184;
  asoout23 n186 = iinp2  & ~n185;
  asoout23 oout20 = ~n183 & ~n186;
  asoout23 n188 = iinp5  & n36;
  asoout23 n189 = ~iinp6  & ~n188;
  asoout23 n190 = ~iinp6  & ~n189;
  asoout23 n191 = iinp0  & ~n190;
  asoout23 n192 = iinp0  & ~n191;
  asoout23 n193 = iinp1  & ~n192;
  asoout23 n194 = ~n165 & ~n193;
  asoout23 n195 = ~iinp2  & ~n194;
  asoout23 oout21 = ~iinp2  & ~n195;
  asoout23 n197 = ~n90 & n179;
  asoout23 n198 = ~iinp2  & ~n197;
  asoout23 oout22 = ~n186 & ~n198;
  asoout23 n200 = ~iinp1  & ~n124;
  asoout23 n201 = iinp1  & ~n96;
  asoout23 n202 = ~n200 & ~n201;
  asoout23 n203 = ~iinp2  & ~n202;
  asoout23 oout24 = ~iinp2  & ~n203;
  asoout23 n205 = ~iinp1  & ~n96;
  asoout23 n206 = ~iinp1  & ~n205;
  asoout23 n207 = ~iinp2  & ~n206;
  asoout23 oout25 = ~iinp2  & ~n207;
  asoout23 oout23 = 1'b1;
endmodule


